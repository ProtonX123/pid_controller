
#include "AS5600.h"

typedef int bool;
enum { false, true };

I2C_HandleTypeDef hi2c1;

int32_t output = 0;
int32_t last_output = 0;
int16_t revolutions = 0;
int16_t offset = 0;
int16_t first = 1;
uint8_t _msb = 0;
uint8_t _lsb = 0;
bool double_register_action = false;
bool msbReceived = false;
bool lsbReceived = false;

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
void I2C1_Init()
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    // todo: Error handler
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

int32_t getPosition()
{
    output = getRegisters2(AS5600_RAWANGLEAddressMSB, AS5600_RAWANGLEAddressLSB);
    if (first)
    {
        last_output = output;
        first = 0;
    }
    if ((output - last_output) < - AS5600_CPR_HALF)
    {
        revolutions++;
    }
    else if ((output - last_output) > AS5600_CPR_HALF)
    {
        revolutions--;
    }
    last_output = output;
    return (output - offset) + AS5600_CPR * revolutions;
}

int16_t getAngle()
{
    return getRegisters2(AS5600_ANGLEAddressMSB, AS5600_ANGLEAddressLSB);
}

int16_t getRawAngle()
{
    return getRegisters2(AS5600_RAWANGLEAddressMSB, AS5600_RAWANGLEAddressLSB);
}

int16_t getStatus()
{
    return getRegister(AS5600_STATUSAddress); //& 0b00111000;
}

void setZero()
{
    revolutions = 0;
    offset = getRegisters2(AS5600_RAWANGLEAddressMSB, AS5600_RAWANGLEAddressLSB);
}

int16_t getRegister(uint8_t reg)
{
    HAL_StatusTypeDef ret = HAL_ERROR;
    _msb = 0;

    // Read MSB
    ret = HAL_I2C_Master_Transmit_IT(&hi2c1, AS5600_ADDR, &reg, 1);
	if( ret != HAL_OK )
	{
      return -1;
	}
	else
	{
		HAL_I2C_Master_Receive_IT(&hi2c1, AS5600_ADDR, &_msb, 1);
	}

	// Wait until msb was received
	while(!msbReceived){}

	return _msb;
}

int32_t getRegisters2(uint8_t registerMSB, uint8_t registerLSB)
{
    HAL_StatusTypeDef ret = HAL_ERROR;
    double_register_action = true;

    _lsb = 0;
    _msb = 0;

    // Read MSB
    ret = HAL_I2C_Master_Transmit_IT(&hi2c1, AS5600_ADDR, &registerMSB, 1);
	if( ret != HAL_OK )
	{
	   return -1;
	}
	else
	{
	    while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY){}

		// Receive function will call the callback as soon as data arrived.
	   HAL_I2C_Master_Receive_IT(&hi2c1, AS5600_ADDR, &_msb, 1);
	}


	// wait until lsb was received
	while(!msbReceived){}


	// First byte has arrived. Get next byte:
    ret = HAL_I2C_Master_Transmit_IT(&hi2c1, AS5600_ADDR, &registerLSB, 1);
	if( ret != HAL_OK )
	{
	   return -1;
	}
	else
	{
	    while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY){}

		// Receive function will call the callback as soon as data arrived.
	   HAL_I2C_Master_Receive_IT(&hi2c1, AS5600_ADDR, &_lsb, 1);
	}


	// wait until lsb was received
	while(!lsbReceived){}


	// Reset flags
	msbReceived = false;
	lsbReceived = false;

	// return result in callback
	return((_lsb) + (_msb & 0x0F) * 256);
}

void HAL_I2C_MasterRxCpltCallback(I2C_HandleTypeDef *hi2c)
{
	UNUSED(hi2c);

	if(double_register_action)
	{
		if(!msbReceived && !lsbReceived)
		{
			msbReceived = true;
		}
		else if(msbReceived && !lsbReceived)
		{
			lsbReceived = true;

			// Reset variable
			double_register_action = false;
		}
		else
		{
			// do nothing
		}
	}
	else
	{
		msbReceived = true;
	}
}

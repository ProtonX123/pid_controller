/*
 * AS5600.h
 *
 *  Created on: Jun 30, 2021
 *      Author: David
 */

#ifndef USER_AS5600_H_
#define USER_AS5600_H_

#include "stm32f4xx_hal.h"

#define AS5600_CPR 					4096
#define AS5600_CPR_HALF 			2048

#define AS5600_ADDR 				0x36 << 1

#define AS5600_ZMCOAddress 			0x00
#define AS5600_ZPOSAddressMSB		0x01
#define AS5600_ZPOSAddressLSB 		0x02
#define AS5600_MPOSAddressMSB 		0x03
#define AS5600_MPOSAddressLSB 		0x04
#define AS5600_MANGAddressMSB 		0x05
#define AS5600_MANGAddressLSB 		0x06
#define AS5600_CONFAddressMSB 		0x07
#define AS5600_CONFAddressLSB 		0x08
#define AS5600_RAWANGLEAddressMSB 	0x0C
#define AS5600_RAWANGLEAddressLSB 	0x0D
#define AS5600_ANGLEAddressMSB 		0x0E
#define AS5600_ANGLEAddressLSB 		0x0F
#define AS5600_STATUSAddress 		0x0B
#define AS5600_AGCAddress 			0x1A
#define AS5600_MAGNITUDEAddressMSB 	0x1B
#define AS5600_MAGNITUDEAddressLSB 	0x1C
#define AS5600_BURNAddress 			0xFF


// Functions
void 		I2C1_Init(void);
int32_t 	getPosition(void);
int16_t 	getAngle(void);
int16_t 	getRawAngle(void);
int16_t 	getStatus(void);
void 		setZero(void);

// Internal (Private)
int32_t     getRegisters2(uint8_t registerMSB, uint8_t registerLSB);
int16_t 	getRegister(uint8_t register1);


#endif /* USER_AS5600_H_ */
